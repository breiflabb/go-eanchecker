package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"
	"time"
)

var total = 0
var countCorrect = 0

func main() {
	start := time.Now()
	file, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	var wg sync.WaitGroup
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		wg.Add(1)
		go checkEan(line, &wg)
	}

	wg.Wait()
	fmt.Printf("\nFound %v correct out of %v checked\n", countCorrect, total)
	end := time.Now()
	fmt.Printf("\n %v eans took: %v\n", total, end.Sub(start))
}

func checkEan(ean string, wg *sync.WaitGroup) {
	defer wg.Done()
	if len(ean) < 13 {
		fmt.Printf("\nDiscard line of length %v [%v]\n", len(ean), ean)
		return
	}
	eInt, _ := strconv.Atoi(ean)
	if eInt == 0 {
		log.Fatal("eINT IT 0")
	}
	step1Chan := make(chan int)
	step3Chan := make(chan int)

	go calculateStep1(eInt, step1Chan)
	go calculateStep3(eInt, step3Chan)
	step1 := <-step1Chan
	step3 := <-step3Chan
	step2 := step1 * 3
	step4 := step2 + step3
	nextTen := (int(step4/10) * 10) + 10
	nearest := nearestHigherOrderOfTen(eInt, step4, nextTen)
	control := nearest - step4
	lastDigit := eInt % 10
	total++
	if control >= 10 {
		control = int(control % 10)
	}
	if control == lastDigit {
		countCorrect++
	}
}
func calculateStep3(ean int, done chan int) {
	step3 := 0
	count := 0
	ean = ean / 100
	for ean > 0 {
		if count%2 == 0 {
			step3 += ean % 10
		}
		ean /= 10
		count++
	}
	done <- step3
}
func calculateStep1(ean int, done chan int) {
	step1 := 0
	count := 0
	for ean > 0 {
		if count%2 != 0 {
			step1 += ean % 10
		}
		ean /= 10
		count++
	}
	done <- step1
}

func nearestHigherOrderOfTen(ean int, s int, nextTen int) int {
	currentClosest := int(^uint(0) >> 1)
	modCheck := 100
	tmpEan := ean
	if nextTen >= 100 {
		modCheck = 1000
	}
	for tmpEan > 0 {
		mod := tmpEan % modCheck
		if mod >= nextTen && mod <= nextTen+9 && mod < currentClosest {
			currentClosest = mod
		}
		tmpEan /= modCheck
	}

	if currentClosest == int(^uint(0)>>1) {
		return nearestHigherOrderOfTen(ean, s, nextTen+10)
	}
	corrected := int(currentClosest/10) * 10
	return corrected
}
